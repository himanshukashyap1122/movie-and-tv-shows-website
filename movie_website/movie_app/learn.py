from pymongo import MongoClient
from flask import Flask, url_for
from flask_pymongo import PyMongo
from flask import Flask, render_template, request,redirect
from keys import Apikey,MONGO_URI,BASE_URL,BASE_IMAGE_URL,BASE_POPULAR_TV,BASE_TV_URL,BASE_SEARCH_URL
import json
import urllib.request as req
API_KEY=Apikey
app = Flask(__name__)
app.config["MONGO_URI"] = MONGO_URI
mongo = PyMongo(app)

client=MongoClient(MONGO_URI)
db = client.get_database("sample_mflix")
records=db.movies
#records.count_documents({"imdb.id":5})
#movie=list(records.find( {"imdb.id": 18773  }))
#movie=list(records.find().sort("released",-1).limit(10));
skip_limit=0
page_limit=20
tv_page=1

#print(movie[1]['genres'])

@app.route('/',methods=['GET','POST'])
#@app.route('/home')
def post_route():
    if(request.method=="POST"):
        name=request.form.get('search')
        return redirect(url_for('route3',file_name=name))
    img_data = mongo.db.images.find()
    img_data = list(img_data)

    total_data=[]
    movie_sorted = list(records.find({'poster': {"$exists": True}}).sort("released", -1).limit(10));
    url = BASE_URL + API_KEY
    conn = req.urlopen(url)
    json_data = json.loads(conn.read())

    total_data.append(img_data)
    total_data.append(movie_sorted)
    total_data.append(json_data['results'])
    return render_template('index.html',movie_sorted=total_data)


@app.route('/movies/<int:imdb>')
#@app.route('/movies',methods=['GET','POST'])
def movies(imdb):
    print(imdb)
    x=records.find( {"imdb.id": imdb})
    return render_template('details.html',movie=x)

@app.route('/latestmovies',methods=['GET','POST'])
def route():
    if (request.method == "POST"):
        name = request.form.get('search')
        return redirect(url_for('route3', file_name=name))
    movie_sorted = list(records.find({'poster': {"$exists": True}}).sort("released", -1).limit(20));
    return render_template('movie.html', movie_sorted=movie_sorted)

@app.route('/nextpage',methods=['GET','POST'])
def route1():
    if (request.method == "POST"):
        name = request.form.get('search')
        return redirect(url_for('route3', file_name=name))
    global skip_limit
    global page_limit
    skip_limit = skip_limit + page_limit;

    movie_sorted = list(records.find({'poster': {"$exists": True}}).sort("released", -1).skip(skip_limit).limit(page_limit));
    return render_template('movie.html', movie_sorted=movie_sorted)

@app.route('/previouspage',methods=['GET','POST'])
def route2():
    if (request.method == "POST"):
        name = request.form.get('search')
        return redirect(url_for('route3', file_name=name))
    global skip_limit
    global page_limit
    skip_limit = skip_limit - page_limit;
    if(skip_limit<0):
        skip_limit=0
    movie_sorted = list(records.find({'poster': {"$exists": True}}).sort("released", -1).skip(skip_limit).limit(page_limit));
    return render_template('movie.html', movie_sorted=movie_sorted)

@app.route('/search/<file_name>',methods=['GET','POST'])
def route3(file_name):
    if (request.method == "POST"):
        name = request.form.get('search')
        return redirect(url_for('route3', file_name=name))
    url = BASE_SEARCH_URL + API_KEY + '&language=en-US&page=1&query=' + file_name + '&include_adult=false'
    conn = req.urlopen(url)
    json_data = json.loads(conn.read())
    #movie_sorted = list(records.find({'poster': {"$exists": True}}).sort("released", -1).skip(skip_limit).limit(page_limit));
    #movie_sorted=list(records.find({"title": {"$regex": "filename"".*substring*.", "$options": "i"}}));
    movie_sorted = list(records.find({"title": {'$regex': file_name, '$options': "$i"}}).sort("released", -1).limit(10));
    total_data = []
    total_data.append(movie_sorted)
    total_data.append(json_data['results'])
    print(movie_sorted)
    return render_template('search.html', movie_sorted=total_data)



# @app.route('/insert')
# def insertimg():
#     return render_template('test.html')
#
# @app.route('/create', methods = ['POST'])
# def create():
#     if 'img' in request.files:
#         prof_img = request.files['img']
#         name = request.form.get('name')
#         print(name)
#         mongo.save_file(prof_img.filename, prof_img)
#         print(prof_img.filename)
#         mongo.db.images.insert({'image_name':prof_img.filename})
#         return "doone"
#
#     return "prof"
@app.route('/getimage/<filename>')
def getimage(filename):
    return mongo.send_file(filename)

@app.route('/tvdetail/<int:tv_id>')
def tvdetail(tv_id):
    url = BASE_TV_URL + str(tv_id) + '?api_key=' + API_KEY
    conn = req.urlopen(url)
    json_data = json.loads(conn.read())
    return render_template('tvdetail.html',data = json_data)


@app.route('/tv', methods=['GET', 'POST'])
def tv():
    if (request.method == "POST"):
        name = request.form.get('search')
        return redirect(url_for('route3', file_name=name))

    url = BASE_POPULAR_TV + API_KEY
    conn = req.urlopen(url)
    json_data = json.loads(conn.read())
    return render_template('tv.html', data=json_data["results"])


@app.route('/tvshows', methods=['GET', 'POST'])
def tvshows():
    if (request.method == "POST"):
        name = request.form.get('search')
        return redirect(url_for('route3', file_name=name))

    url = BASE_POPULAR_TV + API_KEY + '&page=' + str(tv_page)
    conn = req.urlopen(url)
    json_data = json.loads(conn.read())
    return render_template('tv.html', data=json_data["results"])


@app.route('/tv/nextpage')
def nextpage():
    global tv_page
    tv_page = tv_page + 1
    return redirect(url_for('tvshows'))


@app.route('/tv/prevpage')
def prevpage():
    global tv_page
    tv_page = tv_page - 1
    if (tv_page < 1):
        tv_page = 1
    return redirect(url_for('tvshows'))

app.run(debug=True)